module WNS
  class YoutubeTag < Liquid::Tag
    def initialize(tag_name, text, tokens)
      super
      @text = text.strip!.split("|")
    end

    def render(context)
      "<div class='youtube d-grid gap-2' id='" + @text[0] + "'>
      <a data-videoid='" + @text[0] + "'>
      <picture>
      <source srcset='/proxy/youtube/" + @text[0] + "/maxresdefault.webp' width='1280' height='720' type='image/webp'>
      <source srcset='/proxy/youtube/" + @text[0] + "/maxresdefault.jpg' width='1280' height='720' type='image/jpeg'>
      <source srcset='/proxy/youtube/" + @text[0] + "/hqdefault.webp' width='480' height='360' type='image/webp'>
      <source srcset='/proxy/youtube/" + @text[0] + "/hqdefault.jpg' width='480' height='360' type='image/jpeg'>
      <source srcset='/proxy/youtube/" + @text[0] + "/sddefault.webp' width='640' height='480' type='image/webp'>
      <source srcset='/proxy/youtube/" + @text[0] + "/sddefault.jpg' width='640' height='480' type='image/jpeg'>
      <img loading='lazy' src='/proxy/youtube/" + @text[0] + "/hqdefault.jpg' class='mb-2'>
      </picture>
      </a>
  <a class='btn btn-lg btn-block btn-primary youtube-load-btn' data-videoid='" + @text[0] + "'>
  Load YouTube Video (3rd party script)
  </a>
  <a class='btn btn-lg btn-block btn-primary' href='https://youtu.be/" + @text[0] + "' target='_blank' rel='noopener'>
    Watch “" + @text[1] + "” on YouTube
  </a>
</div>
<script id='yt-" + @text[0] + "' type='text/html'>
  <div style='position: relative; padding-bottom: 56.25%; padding-top: 30px; height: 0; overflow: hidden;'>
    <iframe
      style='position: absolute; top: 0; left: 0; width: 100%; height: 100%;'
      src='https://www.youtube-nocookie.com/embed/" + @text[0] + "?rel=0'
      frameborder='0'
      allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture'
      allowfullscreen></iframe>
  </div>
</script>"
    end 
  end
end
